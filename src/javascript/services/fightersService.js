import { callApi } from '../helpers/apiHelper';
import { fightersDetails } from "../helpers/mockData";

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    return fightersDetails.find(f => f._id === id);
  }
}

export const fighterService = new FighterService();
