import { showModal } from "./modal.js";

export function showWinnerModal(fighter) {
  showModal({
    title: "Congratulations!!!",
    bodyElement: ("Winner is " + fighter.name + "!"),
    onClose: () => window.location.reload()
  });
}
