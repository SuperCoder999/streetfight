import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    firstFighter.maxHP = firstFighter.health;
    secondFighter.maxHP = secondFighter.health;

    firstFighter.block = firstFighter.defense;
    secondFighter.block = secondFighter.defense;

    firstFighter.lastCritical = -100;
    secondFighter.lastCritical = -100;

    const pressed = new Set();

    document.body.onkeydown = event => {
      pressed.add(event.code);

      checkBlock({
        blocker: firstFighter,
        blockKey: controls.PlayerOneBlock,
        pressedKeys: pressed
      });

      checkBlock({
        blocker: secondFighter,
        blockKey: controls.PlayerTwoBlock,
        pressedKeys: pressed
      });

      checkAttack({
        attacker: firstFighter,
        defender: secondFighter,
        attackKey: controls.PlayerOneAttack,
        blockKey: controls.PlayerOneBlock,
        pressedKeys: pressed
      });

      checkAttack({
        attacker: secondFighter,
        defender: firstFighter,
        attackKey: controls.PlayerTwoAttack,
        blockKey: controls.PlayerTwoBlock,
        pressedKeys: pressed
      });

      checkCriticalHit({
        attacker: firstFighter,
        defender: secondFighter,
        keys: controls.PlayerOneCriticalHitCombination,
        pressedKeys: pressed,
        attackerBlockKey: controls.PlayerOneBlock
      });

      checkCriticalHit({
        attacker: secondFighter,
        defender: firstFighter,
        keys: controls.PlayerTwoCriticalHitCombination,
        pressedKeys: pressed,
        attackerBlockKey: controls.PlayerTwoBlock
      });

      firstFighter.bar.style.width = Math.max(firstFighter.health / firstFighter.maxHP * 100, 0) + "%";
      secondFighter.bar.style.width = Math.max(secondFighter.health / secondFighter.maxHP * 100, 0) + "%";

      const winner = checkWin(firstFighter, secondFighter);

      if (winner != null) {
        return resolve(winner);
      }
    }

    document.body.onkeyup = event => {
      pressed.delete(event.code);
    }
  });
}

function checkCriticalHit({ attacker, defender, keys, pressedKeys, attackerBlockKey }) {
  let isCriticalCombo = true;
  keys.forEach(key => isCriticalCombo = isCriticalCombo && pressedKeys.has(key));

  if (isCriticalCombo && !pressedKeys.has(attackerBlockKey)) {
    if ((performance.now() / 1000 - attacker.lastCritical) >= 10) {
      attacker.lastCritical = performance.now() / 1000;
      defender.health -= attacker.attack * 2;
    }
  }
}

function checkBlock({ blocker, blockKey, pressedKeys }) {
  if (pressedKeys.has(blockKey)) {
    blocker.block = 9999;
  }
}

function checkAttack({ attacker, defender, attackKey, blockKey, pressedKeys }) {
  if (pressedKeys.has(attackKey) && !pressedKeys.has(blockKey)) {
    defender.health -= getDamage(attacker, defender);
    attacker.block = attacker.defense;
  }
}

function checkWin(first, second) {
  if (first.health <= 0) {
    document.body.onkeydown = () => {};
    document.body.onkeyup = () => {};
    return second;
  }

  if (second.health <= 0) {
    document.body.onkeydown = () => {};
    document.body.onkeyup = () => {};
    return first;
  }

  return null;
}

export function getDamage(attacker, defender) {
  return Math.max(getHitPower(attacker) - getBlockPower(defender), 0);
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.floor(Math.random() * 2) + 1);
}

export function getBlockPower(fighter) {
  return fighter.block * (Math.floor(Math.random() * 2) + 1);
}
