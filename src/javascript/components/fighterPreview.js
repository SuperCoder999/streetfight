import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter) {
    const image = createFighterImage(fighter);

    const nameText = createElement({
      tagName: "h2",
      className: "fighter-preview___info-text"
    })

    const healthText = createElement({
      tagName: "h2",
      className: "fighter-preview___info-text"
    });

    const attackText = createElement({
      tagName: "h2",
      className: "fighter-preview___info-text"
    });

    const defenseText = createElement({
      tagName: "h2",
      className: "fighter-preview___info-text"
    });

    nameText.innerText = fighter.name;
    healthText.innerText = fighter.health + " HP";
    attackText.innerText = "Attack: " + fighter.attack;
    defenseText.innerText = "Defence: " + fighter.defense;
    fighterElement.append(nameText, healthText, image, attackText, defenseText);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
